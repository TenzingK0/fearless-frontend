import React from 'react';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import { BrowserRouter } from 'react-router-dom';
import { Routes, Route, Link } from 'react-router-dom';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <BrowserRouter>
      <Nav />
      <div className="my-5 container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="attendees/new" element={<AttendConferenceForm />} />
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="presentation">
            <Route path="new" element={<PresentationForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
    </>
  );
}

export default App;

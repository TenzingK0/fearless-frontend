function createCard(name, description, pictureUrl, startsMonth, startsDay, startsYear, endsMonth, endsDay, endsYear, location) {
    return `
        <div class="col">
            <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">${startsMonth}/${startsDay}/${startsYear}-${endsMonth}/${endsDay}/${endsYear}</li>
                </ul>
            </div>
        </div>
    `;
}

function makeAlert() {
    return `
    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">Conference not found</h4>
        <p>Sorry but this conference is not avilable right now</p>
    </div>
    `
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            const errorHtml = makeAlert();
            const errorMessage = document.querySelector('.row');
            errorMessage.innerHTML = errorHtml;
            throw new Error('Response not ok');
        } else {
            const data = await response.json();

            for ( let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startsDate = new Date(details.conference.starts);
                    const startsMonth = startsDate.getMonth() + 1;
                    const startsDay = startsDate.getDate();
                    const startsYear = startsDate.getFullYear();
                    const endsDate = new Date(details.conference.ends);
                    const endsMonth = endsDate.getMonth() + 1;
                    const endsDay = endsDate.getDate();
                    const endsYear = endsDate.getFullYear();
                    const location = details.conference.location.name
                    const html = createCard(title, description, pictureUrl, startsMonth, startsDay, startsYear, endsMonth, endsDay, endsYear, location);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                }
            }

        }
    }   catch (e) {
            console.error('error', e)
            const errorHtml = makeAlert();
            const errorMessage = document.querySelector('.row');
            errorMessage.innerHTML = errorHtml;
    }

});
